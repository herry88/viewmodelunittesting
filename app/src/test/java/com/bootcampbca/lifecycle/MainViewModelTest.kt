package com.bootcampbca.lifecycle

import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test


class MainViewModelTest {
    //buat variabel untuk menampung func MainViewModel
    private lateinit var mainViewModel: MainViewModel

    //deklarasikan mainViewModel pada code sebelumnya
    @Before
    fun init() {
        mainViewModel = MainViewModel()
    }

    //lakukan Testing pada bagian ini 
    //result yang di harapkan ada 6, jika tidak error maka hasil perkalian width, length, height benar
    @Test
    fun calculate() {
        val width = "1"
        val length = "2"
        val height = "3"
        mainViewModel.calculate(width, height, length)
        assertEquals(6, mainViewModel.result)
    }
}
